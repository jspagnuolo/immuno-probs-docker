# Fetch latest ubuntu image for the ImmunoProbs image build.
FROM ubuntu:latest

# Install some apt linux packages.
# edited to python3-pip ffrom python-pip
RUN apt-get update \
    && apt-get install -y \
        git \
        gcc-7 \
        g++-7 \
        build-essential \
        muscle \
        python2-dev \ 
        unzip \
        wget \
        curl

RUN curl https://bootstrap.pypa.io/pip/2.7/get-pip.py -o get-pip.py \
    && python2.7 get-pip.py \
    && pip2 --version 

# Download a version of IGoR, unpack it and compile.
RUN wget https://github.com/qmarcou/IGoR/releases/download/1.4.0/igor_1-4-0.zip \
    && unzip igor_1-4-0.zip && cd igor_1-4-0 \
    && ./configure CC=gcc-7 CXX=g++-7 \
    && make clean \
    && make \
    && make install \
    && mv pygor ../pygor \
    && cd .. \
    && rm -r igor_1-4-0 \
    && rm igor_1-4-0.zip 

# Copy and unpack the tutorial data files.
WORKDIR /
COPY tutorial_data.zip /
RUN mkdir -p /tutorial_data \
    && unzip tutorial_data.zip -d /tutorial_data \
    && rm tutorial_data.zip

# Install given version of ImmunoProbs.
#RUN pip install immuno-probs==0.2.1
RUN pip install git+https://bitbucket.org/jspagnuolo/immuno-probs.git
# Specify default setting to be the ImmunoProbs docker image execution.
#WORKDIR /tmp/
#COPY docker_entrypoint.sh /usr/src
#ENTRYPOINT ["/usr/src/docker_entrypoint.sh"]
#CMD ["immuno-probs"]
